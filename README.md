## Мое Docker-приложение

Этот репозиторий содержит код для простого Docker-приложения, которое запускает Netdata и OpenResty. Приложение предназначено для мониторинга и обслуживания трафика к определенному приложению или сервису.

### Компоненты приложения

* Netdata: Инструмент мониторинга производительности в реальном времени, который собирает и отображает показатели производительности различных системных компонентов.
* OpenResty: Высокопроизводительный веб-сервер и обратный прокси на основе Nginx. Он используется для обслуживания интерфейса мониторинга Netdata пользователю.

### Структура развертывания

* docker-compose.yml: Определяет Docker-контейнеры для Netdata и OpenResty, а также их конфигурацию.
* nginx.conf: Конфигурационный файл Nginx для OpenResty. Он устанавливает прокси-переход к контейнеру Netdata.
* playbook.yml: Ansible-плейбук, который автоматизирует развертывание приложения на удаленном хосте.
* inventory.ini: Ansible-файл инвентаризации, который определяет целевой хост для развертывания.

### Использование 

1. Клонируйте этот репозиторий:
      git clone https://github.com/salahov.timurok19/my-docker-app
   
2. Отредактируйте inventory.ini:
   * Замените `<USERNAME>` на имя хоста или IP-адрес вашего сервера.
   * Замените `<HOST_IP>` на фактический IP-адрес вашего удаленного хоста.
   * Сгенерируйте SSH ключ: https://selectel.ru/blog/tutorials/how-to-generate-ssh/
   * Обновите `/home/<USERNAME>/.ssh/<SSH_PUBLIC_KEY>` путем указания пути к вашему SSH-ключу.

3. Запустите Ansible-плейбук:
      `sudo ansible-playbook -i inventory.ini -vvv playbook.yml`
   
   Это развернет Docker-контейнеры и настроит необходимые зависимости на удаленном хосте.

4. Доступ к интерфейсу Netdata:
   * Откройте веб-браузер и перейдите по адресу `http://[server_ip]:8080`.
   * Теперь вы должны увидеть панель управления Netdata.

### Особенности

* Автоматизированное развертывание: Ansible-плейбук упрощает процесс развертывания, обеспечивая согласованность и скорость.
* Контейнерная среда: Запуск приложения в Docker-контейнерах обеспечивает изоляцию, переносимость и простоту управления.
* Мониторинг Netdata: Собирает и отображает важные системные показатели для анализа производительности и устранения неполадок.
* Обратный прокси OpenResty: Обрабатывает входящий трафик и маршрутизирует его к контейнеру Netdata.
